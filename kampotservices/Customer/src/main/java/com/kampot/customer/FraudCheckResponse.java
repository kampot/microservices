package com.kampot.customer;

public record FraudCheckResponse(Boolean isFraudster) {
}
