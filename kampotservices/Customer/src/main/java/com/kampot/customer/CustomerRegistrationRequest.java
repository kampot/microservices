package com.kampot.customer;

public record CustomerRegistrationRequest(
        String firstName,
        String lastName,
        String email) {
}
