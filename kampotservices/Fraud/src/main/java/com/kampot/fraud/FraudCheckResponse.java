package com.kampot.fraud;

public record FraudCheckResponse(Boolean isFraudster) {
}
